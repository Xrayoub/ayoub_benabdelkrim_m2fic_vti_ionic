# IonicApp-Point-de-vente

## Description de l'application

Cette application a été réalisée avec le framework Ionic 3. C'est une application de gestion d'un point de vente. 
L'utilisateur créer la commande du client en cliquant sur les produits qui se trouve dans l'interface "Produits".
Ces produits vont apparaître en bas de l'écran avec leurs prix et leurs quantités. L'utilisateur a la possiblilité
d'ajouter et de retirer des produits en cliquant sur le signe + ou -.
L'utilisateur a aussi la possibilité d'abondonner une commande si le client se désiste. 
Cette application est connectée à une base de données Firebase, qui stocke les transactions émises par l'utilisateur.
Voici les captures d'écran de l'application:
<img src="Screenshots/Image1.png"><br>
<img src="Screenshots/Image2.png"><br>
<img src="Screenshots/Image3.png"><br>
<img src="Screenshots/Image4.png"><br>

Pour se connecter, veuillez utiliser ces identifiants: 
ID: user@gmail.com
MDP: user00

Pour télécharger l'APK, cliquez ici: <a href="https://gitlab.com/Xrayoub/ayoub_benabdelkrim_m2fic_vti_ionic/raw/master/platforms/android/Point_de_vente.apk">Point de vente</a>

