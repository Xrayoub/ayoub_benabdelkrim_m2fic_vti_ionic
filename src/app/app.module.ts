import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TransPage } from '../pages/trans/trans';
import { RegPage } from '../pages/reg/reg';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyAnBs6YRU_A2M4aWEYwEmsaVas9j748A58",
  authDomain: "ionic-project-f8206.firebaseapp.com",
  databaseURL: "https://ionic-project-f8206.firebaseio.com",
  projectId: "ionic-project-f8206",
  storageBucket: "ionic-project-f8206.appspot.com",
  messagingSenderId: "114663080503"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TransPage,
    RegPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TransPage,
    RegPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
