import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';

import { RegPage } from '../reg/reg';
import { TransPage } from '../trans/trans';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;

  order = [];
  countOrders = 0;

drinks = [{
  id: 0,
  name: "Eau",
  price: "2.9",
},
{
  id: 1,
  name: "Jus d'oranges",
  price: "4.0",
},
{
  id: 2,
  name: "Coca Cola",
  price: "3.20",
},
{
  id: 3,
  name: "Cappuccino",
  price: "3.30",
},
{
  id: 4,
  name: "Thé",
  price: "2.90",
},
{
  id: 5,
  name: "Chocolat chaud",
  price: "3.10",
},
{
  id: 6,
  name: "Cacao",
  price: "3.20",
},
{
  id: 7,
  name: "Jus d'avocat",
  price: "4.90",
}];

foods = [{
  id: 8,
  name: "Cake",
  price: "5.50",
},
{
  id: 9,
  name: "Crêpes",
  price: "5.30",
},
{
  id: 10,
  name: "Cheesecake",
  price: "4.70",
},
{
  id: 11,
  name: "Pizza",
  price: "5.70",
},
{
  id: 12,
  name: "Donut",
  price: "2.90",
},
{
  id: 13,
  name: "Escalope",
  price: "6.20",
},
{
  id: 14,
  name: "Burger",
  price: "7.20",
},
{
  id: 15,
  name: "Poisson pané",
  price: "7.50",
}
];


constructor(public fireAuth: AngularFireAuth, public navCtrl: NavController, db: AngularFireDatabase, public alertCtrl: AlertController) {

  this.itemsRef = db.list('/trans/', ref => ref.limitToLast(50),);
  // Use snapshotChanges().map() to store the key
  this.items = this.itemsRef.snapshotChanges().map(changes => {
  return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
  });

  fireAuth.auth.onAuthStateChanged(function(user) {
        if (!user) {
          navCtrl.setRoot(RegPage);
        }


      }); 


}


addToOrderList(item, qty) {
  var flag = 0;

  if (this.order.length > 0) {
      for (var i = 0; i < this.order.length; i++) {
          if (item.id === this.order[i].id) {
              item.qty += qty;
              flag = 1;
              break;
          }
      }
      if (flag === 0) {
          item.qty = 1;
      }
      if (item.qty < 2) {
          this.order.push(item);
      }

  } 
  
  else {
      item.qty = qty;
      this.order.push(item);
  }
}

removeSingleItem (item) {
  for (var i = 0; i < this.order.length; i++) {
      if (item.id === this.order[i].id) {
          item.qty -= 1;
          if (item.qty === 0) {
              this.order.splice(i, 1);
          }
      }
  }
}


removeItem (item) {
  for (var i = 0; i < this.order.length; i++) {
      if (item.id === this.order[i].id) {
          this.order.splice(i, 1);
      }
  }
}

getTotal () {
  var tot = 0;
  for (var i = 0; i < this.order.length; i++) {
      tot += (this.order[i].price * this.order[i].qty)
  }
  return tot;
}

clearOrderList () {
  this.order = [];
}

checkout() {
  this.countOrders += 1;
  let alert = this.alertCtrl.create({ title: "Transaction n° : "+this.countOrders.toString(),
  subTitle: "Prix: €"+this.getTotal().toFixed(2).toString()+" Encaissé!",
  buttons: ['Fermer']});
  alert.present();
  this.itemsRef.push({ orderNum:this.countOrders, total:this.getTotal().toFixed(2) , Date:Date() });
  //this.itemsRef.push(this.order);
  this.order = [];
}

getLastTrans(){
  this.navCtrl.push(TransPage);
}

signOut(){
  this.fireAuth.auth.signOut();
}

}

